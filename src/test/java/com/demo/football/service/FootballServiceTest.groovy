package com.demo.football.service

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class FootballServiceTest extends Specification {

    @Subject
    FootballService footballService = new FootballService()

    @Unroll
    def "FetchLink"() {

        when:
        String result = footballService.fetchLink(nazwa)

        then:

        nazwaStony == result

        where:
        nazwa        | nazwaStony
        "West Ham"   | "https://en.wikipedia.org/wiki/West_Ham_United_F.C."
        "Arsenal"    | "https://en.wikipedia.org/wiki/Arsenal_F.C."
        "Liverpool"  | "https://en.wikipedia.org/wiki/Liverpool_F.C."
        "Chelsea"    | "https://en.wikipedia.org/wiki/Chelsea_F.C."
        "Bayern"     | "https://en.wikipedia.org/wiki/FC_Bayern_Munich"


    }
}
