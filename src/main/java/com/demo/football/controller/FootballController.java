package com.demo.football.controller;

import com.demo.football.service.FootballService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class FootballController {

    private final FootballService footballService;


    @RequestMapping(value = "/football", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> listAllUsers(@RequestParam String name) {
        return new ResponseEntity<String>(footballService.fetchLink(name) , HttpStatus.OK);
    }
}
