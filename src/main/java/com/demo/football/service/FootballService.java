package com.demo.football.service;

import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Component
public class FootballService {

    public String fetchLink(String teamName){
        String teamPageId = getTeamPageId(teamName);
        return getTeamUrl(teamPageId);
    }

    private String getTeamPageId(String teamName){
        String url = "https://en.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=%\"" + teamName + "\"%&srlimit=1";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);
        Optional<String> title = Arrays.stream(Objects.requireNonNull(forEntity.getBody()).split(",")).filter(s -> s.contains("pageid")).findFirst();

        if(title.isPresent()){
            return Arrays.stream(title.get().split(":")).reduce((first, second) -> second).orElse(null);
        }
        throw new IllegalArgumentException("Illegal team name");
    }

    @SneakyThrows
    private String getTeamUrl(String teamPageId) {

        String url = "https://en.wikipedia.org/w/api.php?action=query&prop=info&pageids=" + teamPageId + "&inprop=url";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);

        String html = Arrays.stream(Objects.requireNonNull(forEntity.getBody()).split("&quot;</span>")).filter(c -> !c.contains("html")).reduce((first, second) -> second).orElse(null);
        return Arrays.stream(html.split(";")).reduce((first, second) -> second).orElse(null);
    }
}
